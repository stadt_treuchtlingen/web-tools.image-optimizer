# web-tools.image-optimizer

This script is licensed under the [AGPL-3.0 License](LICENSE).

By default it applies lossless image optimization to all png and jpeg images
(currently by file extension to improve performance) to all image files it finds
inside a given directory.

Dependencies:
* `optipng`
* `jpegoptim`

Both can be found under this name in most package repositories.

## Why?

One may ask why an external script and not an addon for the CMS?

The answer is: Every addon adds complexity to your CMS and makes it more difficult to maintain, this script runs independent of your CMS and save you some headaces this way.

## Using it

Basic usage: `optimize_images.sh --dir /path/to/image/directory/`

*Warning:* Make sure to run it as the user that owns the images, for a CMS on Debian this is likely the `www-data` user, otherwise you may have problems with your CMS no longer being able to modify or access the optimized files.

To avoid problems in such a scenario use it like: `sudo -u www-data optimize_images.sh --dir /path/to/image/directory/`

## Cronjob

Because a CMS with multiple people working on it has to deal with unoptimized images on a regular basis it makes sense to run this script as a cronjob.

`sudo -u www-data crontab -e`

And add a line that looks similar to this one:
```
# For more information see the manual pages of crontab(5) and cron(8)
# m h  dom mon dow   command
42 8 * * 5 optimize_images.sh --dir /path/to/image/directory/
```

This example will run it every Friday morning on 8:42.

To make your life easier: [Here is a link to the Debian crontab(5) manpage](https://manpages.debian.org/bullseye/cron/crontab.5.en.html).