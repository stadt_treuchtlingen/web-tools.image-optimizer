#!/bin/bash

# Copyright (C) 2023 Stadt Treuchtlingen
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License along with this program. 
# If not, see <https://www.gnu.org/licenses/>

DRY_RUN=""
DIRECTORY=""
OPTIPNG_LEVEL="5"

show_help() {

cat <<EOF
Usage: compress_images.sh <directory>
Lossless compression of jpeg and png file
using jpegoptim and optipng

OPTIONS
	--dir <directory>
		The Directory to compress images in
	--dry-run
		Don't actually run the compression programs
	--optipng-level <level>
		Run optipng with -o <level>, default is 5
EOF

}

while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--dir) DIRECTORY="$2"; shift 2;;
		--dry-run) DRY_RUN="y"; shift 1;;
		--optipng-level) OPTIPNG_LEVEL="$2"; shift 2;;

		--help) show_help; exit 0;;
		*) printf "Unknown option: %s\n" "$1"; exit 1;;
	esac
done

if [ -z "$DIRECTORY" ] ; then
	echo "Please specify a directory to compress using --dir" >&2
	exit 1
fi

find "$DIRECTORY" -iname "*.png" -or -iname "*.jpg" -or -iname "*.jpeg" | while read filename ; do
    if printf "%s\n" "$filename" | grep -q '.png$' ; then
	printf "PNG: %s\n" "$filename"
	[ -n "$DRY_RUN" ] || optipng -preserve -o "$OPTIPNG_LEVEL" -- "$filename"
    else
	printf "JPEG: %s\n" "$filename"
	[ -n "$DRY_RUN" ] || jpegoptim -p -- "$filename"
    fi
done